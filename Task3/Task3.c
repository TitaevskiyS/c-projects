#ifdef _WIN32
	#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#define	EXIT_WITH_ERROR	-1

/*
 * Определение строки формата ввода для size_t
 */
#ifdef WIN64
	#define FORMAT_STRING "%Id %Id"
#else
	#define FORMAT_STRING "%zd %zd"
#endif

int* Shift(int* input_array, const size_t row, const size_t col)
{
	int* output_array = (int*) calloc(row * col, sizeof(int));
	if (!output_array) {
		return NULL;													//error allocated memory - exit
	}

	if (col == 1) {
		memcpy(output_array, input_array, sizeof(int) * row);			//частный случай
		return output_array;
	}

	int res = 0;
	for (size_t i = 0; i < row; ++i) {
		res = 0;
		for (size_t j = 0; j < col; ++j) {
			res += input_array[i * col + j];							//сумма элементов i строки
		}

		if (res % 2) {													//нечетная res % 2 == 1 - сдвиг вправо

			output_array[i * col] = input_array[i * col + col - 1];		//последний -> первый
			memcpy(&output_array[i * col + 1],
					&input_array[i * col],
					(col - 1) * sizeof(int));							//перенос остатка
		}
		else {															//четная res % 2 == 0 - сдвиг влево

			output_array[i * col + col - 1] = input_array[i * col];		//первый -> последний
			memcpy(&output_array[i * col],
					&input_array[i * col + 1],
					(col - 1) * sizeof(int));							//перенос остатка
		}
	}

	return output_array;
}

int main()
{
	size_t row = 0;
	size_t col = 0;
	int* input_array = NULL;
	int* output_array = NULL;
	int error = 0;

	error = scanf(FORMAT_STRING, &row, &col);
	if (error != 2 || row <= 0 || col <= 0) {
		return 0;												//wrong input - exit
	}

	input_array = (int*) calloc(row * col, sizeof(int));
	if (!input_array) {
		return 0;												//error allocated memory - exit
	}

	for (size_t i = 0; i < row * col; ++i) {
		error = scanf("%d", &input_array[i]);
		if (error != 1) {
			error = EXIT_WITH_ERROR;							//set error = -1 (don't work, free allocated space, exit)
			break;
		}
	}

	output_array = Shift(input_array, row, col);

	if (output_array)
		for (size_t i = 0; i < row; ++i) {
			for (size_t j = 0; j < col; ++j) {
				printf("%d ", output_array[i * col + j]);
			}
			printf("\n");
		}

	/*
	 * Free memory
	 */
	free(output_array);
	free(input_array);

#ifdef _WIN32
	system("Pause");
#endif

	return 0;
}
