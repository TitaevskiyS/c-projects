#include <pthread.h>
#include <iostream>
#include <sstream>
#include <queue>
#include <string>
#include <vector>
#include <stdlib.h>
#include <random>
#include <ctime>

using namespace std;

queue<string> chat_buff;

vector<pthread_t> clients;
vector<int> client_id;

pthread_t server;

pthread_mutex_t mutex;
pthread_cond_t new_message;

string Get_Client_Message(int id)
{
	stringstream str;
	time_t now = time(NULL);

	str << ctime(&now);
	str << "Hi, I'm bot #" << id << '\n';

	string temp(str.str());

	return temp;
}

void* Bot(void* me)
{
	string message;
	int id = *(int*)me;

	while (true) {
		sleep(rand() % 20 + 1);

		pthread_mutex_lock(&mutex);

		message = Get_Client_Message(id);
		chat_buff.push(message);
		pthread_cond_signal(&new_message);

		pthread_mutex_unlock(&mutex);
	}

	return NULL;
}

void* User(void* none)
{
	string message;

	while (true) {
		getline(cin, message);

		pthread_mutex_lock(&mutex);

		chat_buff.push(message);
		pthread_cond_signal(&new_message);

		pthread_mutex_unlock(&mutex);
	}

	return NULL;
}

void* Server(void* none)
{
	string temp;
	bool bf_exit = false;

	while (!bf_exit) {
		pthread_mutex_lock(&mutex);

		pthread_cond_wait(&new_message, &mutex);
		while (chat_buff.size() > 0)
		{
			if (chat_buff.front() == "exit") {
				bf_exit = true;
			}
			cout << chat_buff.front();
			cout << "\n";
			chat_buff.pop();
		}
		pthread_mutex_unlock(&mutex);
	} while (!bf_exit);

	for (size_t i = 0; i < clients.size(); ++i)
		pthread_cancel(clients[i]);

	return NULL;
}

int Init_chat(const int client_count)
{
	if (client_count > 0) {
		srand(time(0));

		if (pthread_cond_init(&new_message, NULL)) {
			cout << "Can't init a cond\n";
			return EXIT_FAILURE;
		}

		if (pthread_mutex_init(&mutex, NULL)) {
			cout << "Can't init a mutex\n";
			return EXIT_FAILURE;
		}

		if (pthread_create(&server, NULL, Server, NULL)) {
			cout << "Can't create a server\n";
			return EXIT_FAILURE;
		}

		clients.resize(client_count);
		client_id.resize(client_count);

		client_id[0] = 0;
		if (pthread_create(&clients[0], NULL, User, &client_id[0])) {
			cout << "Can't create a user\n";
			return EXIT_FAILURE;
		}
		pthread_detach(clients[0]);

		for (int i = 1; i < client_count; ++i) {
			client_id[i] = i;
			if (pthread_create(&clients[i], NULL, Bot, &client_id[i])) {
				cout << "Can't create a client\n";
				return EXIT_FAILURE;
			}
			pthread_detach(clients[i]);
		}
	}

	return EXIT_SUCCESS;
}

int End_chat()
{
	if (pthread_join(server, NULL)) {
		cout << "Can't join a server\n";
		return EXIT_FAILURE;
	}

	if (pthread_mutex_destroy(&mutex)) {
		cout << "Can't destroy a mutex\n";
		return EXIT_FAILURE;
	}

	if (pthread_cond_destroy(&new_message)) {
		cout << "Can't destroy a cond\n";
		return EXIT_FAILURE;
	}

	cout << "Chat end, bye!\n";
	return EXIT_SUCCESS;
}
