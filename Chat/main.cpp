#include <iostream>
#include <stdlib.h>

using namespace std;

int Init_chat(const int client_count);
int End_chat();

int main()
{
	int client_count = 0;

	cout << "Input client count (> 0): ";
	cin >> client_count;

	if (cin.fail()) {
		cout << "Wrong input!/n";
		return EXIT_FAILURE;
	}

	if (Init_chat(client_count) == EXIT_FAILURE)
		return EXIT_FAILURE;

	if (End_chat() == EXIT_FAILURE)
		return EXIT_FAILURE;

	return EXIT_SUCCESS;
}

