#ifdef _WIN32
	#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdio.h>
#include <stdlib.h>

#define	EXIT_WITH_ERROR	-1

/*
 * Определение строки формата ввода для size_t
 */
#ifdef WIN64
	#define FORMAT_STRING "%Id"
#else
	#define FORMAT_STRING "%zd"
#endif

int gcd(int a, int b)	//Euclide's algorithm
{
	if (!(a&&b))
		return 0;
	while (a&&b)
		if (abs(a) > abs(b))
			a %= b;
		else
			b %= a;
	return abs(a + b);
}

int** Coprime(int** input_array, const size_t array_size)
{
	if (array_size < 2) {
		return NULL;				//coprime values may be only in array, with size >= 2
	}

	/*
	 * Count pair for array_size:
	 * count = array_size * array_size - array_size
	 * /2 for uniq pairs
	 * *2 for elements
	 * +1 for NULL-terminated symbol (NULL)
	 */
	size_t max_count_coprime = array_size * array_size - array_size + 1;

	int** res = (int**)calloc(max_count_coprime, sizeof (int*));
	if (!res) {
		return NULL;
	}

	int current_pos = 0;	//in res array

	for (size_t i = 0; i < array_size; ++i) {
		for (size_t j = i + 1; j < array_size; ++j) {
			if (gcd(*input_array[i], *input_array[j]) == 1) {		//coprime values have gcd == 1
				res[current_pos] = input_array[i];
				res[current_pos + 1] = input_array[j];
				current_pos += 2;
			}
		}
	}

	return res;
}

int main()
{
	int error = 0;
	size_t array_size = 0;
	int** input_array = NULL;
	int** output_array = NULL;
	int current_pos = 0;	//in output_array

	error = scanf(FORMAT_STRING, &array_size);
	if (error != 1) {
		return 0;												//wrong input - exit
	}

	input_array = (int**)calloc(array_size, sizeof (int*));
	if (!input_array){
		return 0;												//error allocated memory - exit
	}

	for (size_t i = 0; i < array_size; ++i) {
		input_array[i] = (int*)malloc(sizeof (int*));
		if (!input_array[i]) {
			error = EXIT_WITH_ERROR;							//set error = -1 (don't work, free allocated space, exit)
			break;
		}

		error = scanf("%d", input_array[i]);
		if (error != 1) {
			error = EXIT_WITH_ERROR;							//set error = -1 (don't work, free allocated space, exit)
			break;
		}
	}

	if (error != EXIT_WITH_ERROR) {								//if all good - work
		output_array = Coprime(input_array, array_size);

		while (output_array[current_pos] != NULL){				//output_array[current_pos + 1] != NULL - ???
			printf("%d %d\n", *output_array[current_pos], *output_array[current_pos + 1]);
			current_pos += 2;
		}
	}

/*
 * Free memory
 */
	free(output_array);
	for (int i = 0; i < array_size; ++i) {
		free(input_array[i]);
	}
	free(input_array);

#ifdef _WIN32
	system("Pause");
#endif

	return 0;
}
