#include <thread>
#include <chrono>
#include <mutex>
#include <condition_variable>

#include <iostream>
#include <sstream>

#include <queue>
#include <string>

#include <stdlib.h>
#include <random>
#include <ctime>

using namespace std;

queue<string> chat_buff;

mutex use_queue;
condition_variable new_message;

string Get_Client_Message(int id)
{
	stringstream str;
	time_t now = time(NULL);

	str << ctime(&now);
	str << "Hi, I'm bot #" << id << '\n';

	string temp(str.str());

	return temp;
}

void Bot(int id)
{
	string message;

	while (true) {
		this_thread::sleep_for(chrono::seconds(rand() % 15 + 1));

		unique_lock<mutex> lock(use_queue);

		message = Get_Client_Message(id);
		chat_buff.push(message);

		new_message.notify_one();
	}
}

void User()
{
	string message;

	while (true) {
		getline(cin, message);

		unique_lock<mutex> lock(use_queue);

		chat_buff.push(message);

		new_message.notify_one();
	}
}

void Server()
{
	string temp;
	bool bf_exit = false;

	while (!bf_exit) {
		unique_lock<mutex> lock(use_queue);

		new_message.wait(lock);

		while (chat_buff.size() > 0)
		{
			if (chat_buff.front() == "exit") {
				bf_exit = true;
			}
			cout << chat_buff.front();
			cout << "\n";
			chat_buff.pop();
		}
	} while (!bf_exit);
}

int main()
{
	srand(time(0));

	cout << "Welcome to chat!\n";

	thread server(Server);

	thread user(User);
	user.detach();

	thread bot1(Bot, 1);
	bot1.detach();

	thread bot2(Bot, 2);
	bot2.detach();

	server.join();

	cout << "Chat end, bye!\n";
	return EXIT_SUCCESS;
}

