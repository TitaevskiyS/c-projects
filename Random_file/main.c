#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int main()
{
	short temp = 0;
	int count = 15000000;
	FILE* file;

	srand(time(0));

	file = fopen("2.txt", "w");

	if (file) {
		for (int var = 0; var < count; ++var) {
			temp = rand() / 65535;
			fprintf(file, "%d\n", temp);
		}

		fclose(file);
	}
	else {
		printf("Error\n");
		return EXIT_FAILURE;
	}

	printf("Success\n");
	return EXIT_SUCCESS;
}

