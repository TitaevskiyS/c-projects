#ifdef _WIN32
	#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define PARAM_COUNT	4

double Solve(double a, double b, double c, double d, int x)
{
	return (a * x * x * x + b * x * x + c * x + d);
}

int* IntRoot(const double* const a, const double* const b, const double* const c, const double* const d)
{
	double del = 0;							//delimiter
	int* res = NULL;
	int i = 0;

	if (!*d){
		res = (int*)malloc(sizeof (res));
		if (!res)
			return NULL;
		*res = 0;							//d == 0 , root == 0
		return res;
	}

	if (*a){
		del = *a;
	}
	else if (*b){
		del = *b;
	}
	else if (*c){
		del = *c;
	}										//

	if (!del)								//a == 0, b == 0, c == 0, d != 0 - no roots
		return NULL;

	if (fmod(*d, del))						//root is the delimiter d, no int root
		return NULL;

	int q = abs((int)(*d / del));

	for (i = -q; i < 0; ++i)				//[-|q|, |q|] may contain int root, look for [-|q|, 0) and take i and -i
	if (!(q%i)){							//root is the delimiter q
		if (!Solve(*a, *b, *c, *d, i)){
			res = (int*)malloc(sizeof (res));
			if (!res)
				return NULL;
			*res = i;						// root == i
			return res;
		}
		else if (!Solve(*a, *b, *c, *d, -i)){
			res = (int*)malloc(sizeof (res));
			if (!res)
				return NULL;
			*res = -i;						//root == -i
			return res;
		}
	}
	return NULL;
}

int main()
{
	double	a = 0.,
			b = 0.,
			c = 0.,
	 	 	d = 0.;
	int* root = NULL;
	int read_count = 0;

	read_count = scanf("%lf %lf %lf %lf", &a, &b, &c, &d);

	if (read_count == PARAM_COUNT)
		root = IntRoot(&a, &b, &c, &d);

	if (root)
		printf("%d", (int)*root);
	else
		printf("NULL");

	free(root);

#ifdef _WIN32
	printf("\n");
	system("Pause");
#endif

	return 0;
}
