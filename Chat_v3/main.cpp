#include <iostream>
#include <stdlib.h>
#include <dlfcn.h>

#ifdef _WIN32
	#include <Windows.h>
#endif

using namespace std;

//int Init_chat(const int client_count);
//int End_chat();

int main()
{
	int client_count = 0;
	void* lib = NULL;
	int (*init)(const int) = NULL;
	int (*end)(void) = NULL;

	cout << "Input client count (> 0): ";
	cin >> client_count;

	if (cin.fail()) {
		cout << "Wrong input!/n";
		return EXIT_FAILURE;
	}

	lib = dlopen("libchat.so", RTLD_LAZY);
	if (!lib) {
		cout << dlerror() << "\nCan't load lib\n";
		return EXIT_FAILURE;
	}

	init = (int (*)(const int))dlsym(lib, "Init_chat");
	if (!init) {
		cout << dlerror();
		cout << "\nCan't load Init\n";
		dlclose(lib);
		return EXIT_FAILURE;
	}

	end = (int (*)(void))dlsym(lib, "End_chat");
	if (!end) {
		cout << dlerror() << "\nCan't load End\n";
		dlclose(lib);
		return EXIT_FAILURE;
	}

	cout << (*init)(client_count);
	cout << end();

	dlclose(lib);

	return EXIT_SUCCESS;
}

